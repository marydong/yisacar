#!/usr/bin/env python
import logging
import sys,datetime,timeit
import uuid
log = logging.getLogger()
log.setLevel('DEBUG')
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
log.addHandler(handler)

from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement

KEYSPACE = "yisa_oe"


def main1():
    cluster_old = Cluster(['172.22.0.73','172.22.0.74'])
    cluster_new = Cluster(['172.22.0.75','172.22.0.76'])
    session_old = cluster_old.connect()
    session_new = cluster_new.connect()

    log.info("setting keyspace...")
    session_old.set_keyspace(KEYSPACE)
    session_new.set_keyspace(KEYSPACE)
    today = datetime.datetime.now()
    for d in xrange(-180,30):
        day = today + datetime.timedelta(days = d)
        ds = day.strftime('%Y%m%d')
        if int(ds)<=20170426:
            continue
        ds = '20170511'
        print 'export table',ds
        table_name = 'd_%s' % ds
        future_old = session_old.execute_async("SELECT * FROM %s" % table_name)
        start = timeit.default_timer()
        try:
            rows = future_old.result()
        except Exception:
            log.exception("Error reading rows:")
            return

        # query = SimpleStatement("""
            # INSERT INTO %(t)s (info_id,capture_time,data_bin,region_id,location_id,location_type_id,device_id,lane_id,direction_id,speed,license_plate,lp_type,image_url1,has_image,last_captured)
            # VALUES (%(info_id)s, %(capture_time)d, %(data_bin)s, %(region_id)d, %(location_id)d, %(location_type_id)d, %(device_id)d, %(lane_id)d, %(direction_id)d, %(speed)d, %(license_plate)s, %(lp_type)d, %(image_url1)s, %(has_image)d, %(last_captured)s)
            # """, consistency_level=ConsistencyLevel.ONE)

        prepared = session_new.prepare("""
            INSERT INTO %s (info_id,capture_time,data_bin,region_id,location_id,location_type_id,device_id,lane_id,direction_id,speed,license_plate,lp_type,image_url1,has_image,last_captured)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            """ % table_name)
        c = 0
        for row in rows:
            # print row
            #session_new.execute(query, dict(t=table_name,info_id=row.info_id, capture_time=row.info_id, data_bin=row.info_id, region_id=row.info_id, location_id=row.info_id, location_type_id=row.info_id, device_id=row.info_id, lane_id=row.info_id, direction_id=row.info_id, speed=row.info_id, license_plate=row.info_id, lp_type=row.info_id, image_url1=row.info_id, has_image=row.info_id, last_captured=row.info_id))
            session_new.execute(prepared, (row.info_id,row.capture_time, row.data_bin, row.region_id, row.location_id, 0, row.device_id, row.lane_id, row.direction_id, row.speed, row.license_plate, row.lp_type, row.image_url1, 0, row.last_captured))
            c+=1
        print 'row ',c,
        end = timeit.default_timer()
        print 'use time',end-start
        sys.exit(1)

def main2():
    cluster_old = Cluster(['172.22.0.73','172.22.0.74'])
    cluster_new = Cluster(['172.22.0.75','172.22.0.76'])
    session_old = cluster_old.connect()
    session_new = cluster_new.connect()

    log.info("setting keyspace...")
    session_old.set_keyspace(KEYSPACE)
    session_new.set_keyspace(KEYSPACE)
    today = datetime.datetime.now()
    for d in xrange(-180,30):
        day = today + datetime.timedelta(days = d)
        ds = day.strftime('%Y%m%d')
        if int(ds)<=20170426:
            continue
        #ds = '20170512'
        print 'export table',ds,
        table_name = 'today_%s' % ds
        start = timeit.default_timer()
        try:
            future_old = session_old.execute_async("SELECT * FROM %s" % table_name)
            rows = future_old.result()
        except Exception:
            log.exception("Error reading rows:")
            continue

        # query = SimpleStatement("""
            # INSERT INTO %(t)s (info_id,capture_time,data_bin,region_id,location_id,location_type_id,device_id,lane_id,direction_id,speed,license_plate,lp_type,image_url1,has_image,last_captured)
            # VALUES (%(info_id)s, %(capture_time)d, %(data_bin)s, %(region_id)d, %(location_id)d, %(location_type_id)d, %(device_id)d, %(lane_id)d, %(direction_id)d, %(speed)d, %(license_plate)s, %(lp_type)d, %(image_url1)s, %(has_image)d, %(last_captured)s)
            # """, consistency_level=ConsistencyLevel.ONE)

        prepared = session_new.prepare("""
            INSERT INTO %s (s,t_h,t_m,info_id,capture_time,data_bin,region_id,location_id,location_type_id,device_id,lane_id,direction_id,speed,license_plate,lp_type,image_url1,has_image,last_captured)
            VALUES (?,?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            """ % table_name)
        c = 0
        for row in rows:
            # print row
            #session_new.execute(query, dict(t=table_name,info_id=row.info_id, capture_time=row.info_id, data_bin=row.info_id, region_id=row.info_id, location_id=row.info_id, location_type_id=row.info_id, device_id=row.info_id, lane_id=row.info_id, direction_id=row.info_id, speed=row.info_id, license_plate=row.info_id, lp_type=row.info_id, image_url1=row.info_id, has_image=row.info_id, last_captured=row.info_id))
            session_new.execute(prepared, (row.s,row.t_h,row.t_m,row.info_id,row.capture_time, row.data_bin, row.region_id, row.location_id, 0, row.device_id, row.lane_id, row.direction_id, row.speed, row.license_plate, row.lp_type, row.image_url1, 0, row.last_captured))
            c+=1
        print 'row ',c,
        end = timeit.default_timer()
        print 'use time',end-start
        #sys.exit(1)
if __name__ == "__main__":
    main1()
