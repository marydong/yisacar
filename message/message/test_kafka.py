#!/usr/bin/env python
# -*- coding:utf-8 -*-
from kafka import KafkaProducer
from kafka.errors import (
    FailedPayloadsError, ConnectionError, RequestTimedOutError,
    NotLeaderForPartitionError)
import redis,json,yaml
import MySQLdb
import sys,os,datetime,time

if __name__ == "__main__":
    config_file = open(os.path.dirname(os.path.abspath(__file__)) + '/config.yaml')
    config = yaml.safe_load(config_file)
    config_file.close()

    db = MySQLdb.connect(host=config['mysql_web']['host'],port=config['mysql_web']['port'], user=config['mysql_web']['user'], passwd=config['mysql_web']['password'], db=config['mysql_web']['database'],charset="utf8")
    mysql_web = db.cursor(MySQLdb.cursors.DictCursor)
    mysql_web.execute("SELECT location_id,loc_id FROM mon_location;")
    rows = mysql_web.fetchall()
    L2L = {}
    for row in rows:
        L2L[row['location_id']]=row['loc_id']
    mysql_web.close()
    db.close()

    r = redis.StrictRedis(unix_socket_path='/dev/shm/redis_pubsub.sock')
    p = r.pubsub(ignore_subscribe_messages=True)
    topic = 'BIGDATA'
    p.subscribe(topic)
    #-----------------------------
    # while True:
        # message = p.get_message()
        # if message:
            # data = message['data']
            # print data
    #-----------------------------
    kafka_topic = "recognitionMessage"
    producer = KafkaProducer(bootstrap_servers='com1:9092,com2:9092,com3:9092,com4:9092,com5:9092')
    
    #{"region_id": 370211, "brand_id": 135, "lane_id": 2,  "info_id": "5902a89f-6d47-e416-cb02-f7b4273e0d0d", "model_id": 1296, "device_id": 370211000213000173, "source_id": 1,}
    c=0
    for message in p.listen():
        data = message['data']
        #print data
        json_r = json.loads(data)
        send_data = {}
        send_data['nodeId'] = "huangdao"
        send_data['partType'] = json_r['source_id']
        send_data['levelId'] = json_r['level_id']
        send_data['yearId'] = json_r['year_id']
        send_data['yearIdProb'] = json_r['year_id_prob']
        send_data['plateNumber'] = json_r['license_plate']
        dt = datetime.datetime(*time.strptime(json_r['capture_time'], '%Y-%m-%d %H:%M:%S')[:6])
        ti = int(time.mktime(dt.timetuple()))    
        send_data['captureTime'] = ti
        if json_r['color_id']:
            send_data['colorId'] = json_r['color_id'][0]
        else:
            send_data['colorId'] = -1
        send_data['directionId'] = json_r['direction_id']
        send_data['plateCategoryId'] = json_r['plate_type_id2']
        if json_r['detection']:
            send_data['carBox'] = str(json_r['detection']['x'])+","+str(json_r['detection']['y'])+","+str(json_r['detection']['w'])+","+str(json_r['detection']['h'])
        else:
            send_data['carBox'] = ''
        send_data['speed'] = json_r['speed']
        send_data['recPlateNumber'] = json_r['license_plate2']
        send_data['locationId'] = L2L.get(json_r['location_id'])
        send_data['feature'] = json_r['feature_b64']
        send_data['skylight'] = json_r['sunroof']
        send_data['baggageHold'] = json_r['roof_rack']
        send_data['sprayWord'] = json_r['graffiti']
        send_data['inspectionTag'] = json_r['num_certificates']
        send_data['sunShadeLeft'] = json_r['sun_visor_left']
        send_data['sunShadeRight'] = json_r['sun_visor_right']
        send_data['pendant'] = json_r['pendant']
        send_data['tissueBox'] = json_r['tissue']
        send_data['decoration'] = json_r['accessory']
        send_data['card'] = json_r['card']
        send_data['personLeft'] = json_r['occupant_left']
        send_data['personRight'] = json_r['occupant_right']
        send_data['backBurnerTail'] = json_r['spare_tire']
        send_data['imageUrl'] = json_r['image_url1']
        send_data['sendTime'] = int(time.time())
        try:
            producer.send(kafka_topic, json.dumps(send_data))
        except NotLeaderForPartitionError,nlfpe:
            print str(nlfpe)
            time.sleep(1)
        except FailedPayloadsError,fpe:
            print str(fpe)
            print send_data
            time.sleep(0.1)
        c+=1
        if c%100==0:
            print str(datetime.datetime.now()),c
    producer.stop()
    client.close()
    p.unsubscribe()
    p.close()




# from kafka import KafkaClient,SimpleClient,SimpleConsumer,SimpleProducer, KeyedProducer
# from kafka.errors import (
    # FailedPayloadsError, ConnectionError, RequestTimedOutError,
    # NotLeaderForPartitionError)
# from kafka.producer.base import Producer
# from kafka.structs import TopicPartition
# import redis,json
# import sys
# topic = "recognitionMessage"
# client = KafkaClient(hosts=["172.22.0.47:9092"], timeout=30)
# consumer = SimpleConsumer(client,"python",topic)
# print consumer.get_messages(5)
# for msg in consumer:
    # print(msg)
